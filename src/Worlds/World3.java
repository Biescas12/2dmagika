package Worlds;

import java.awt.event.KeyEvent;

import Game.Entities.Creatures.Bowser;
import Game.Entities.Creatures.Ghost;
import Game.Entities.Creatures.Player;
import Game.Entities.Statics.Tree;
import Game.GameStates.State;
import Main.Handler;

public class World3 extends BaseWorld { //Boss world.

	private Handler handler;
	private Player player;

	public World3(Handler handler, String path, Player player) {

		super(handler, path, player);
		this.player = player;
		this.handler = handler;

		if(handler.getKeyManager().keyJustPressed(KeyEvent.VK_N)) {
			State.setState(handler.getGame().gameWinState);
		}

		entityManager.addEntity(new Ghost (handler, 500, 500));
		entityManager.addEntity(new Bowser (handler, 300, 700));
		entityManager.addEntity(new Bowser (handler, 400, 400));
		entityManager.addEntity(new Bowser (handler, 700, 400));
		entityManager.addEntity(new Bowser (handler, 800, 600));
		entityManager.addEntity(new Tree (handler, 300, 150));
		entityManager.addEntity(new Tree (handler, 800, 300));
		entityManager.addEntity(new Tree (handler, 510, 640));
		entityManager.addEntity(new Tree (handler, 200, 750));


	}


}

