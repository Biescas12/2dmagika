package Worlds;

import Game.Entities.Creatures.Goblin;
import Game.Entities.Creatures.Player;
import Game.Entities.Statics.Door;
import Game.Entities.Statics.Rock;
import Game.Entities.Statics.Sign;
import Main.Handler;

/**
 * Created by Elemental on 2/10/2017.
 */
public class CaveWorld extends BaseWorld {
	private Handler handler;
	private Player player;
	private BaseWorld world3;

	public CaveWorld(Handler handler, String path, Player player) {
		super(handler, path, player);
		this.handler = handler;
		this.player = player;
		world3 = new World3(handler, "res/Maps/Map3.map", player);

		entityManager.addEntity(new Sign(handler, 100, 150));
		entityManager.addEntity(new Rock(handler, 100, 450));
		entityManager.addEntity(new Rock(handler, 984, 120));
		entityManager.addEntity(new Rock(handler, 740, 900));
		entityManager.addEntity(new Rock(handler, 1200, 670));
		entityManager.addEntity(new Door(handler, 200, 0, world3));
		entityManager.addEntity(new Rock(handler, 654, 846));
		entityManager.addEntity(new Rock(handler, 1300, 160));
		entityManager.addEntity(new Rock(handler, 150, 350));
		entityManager.addEntity(new Rock(handler, 914, 351));
		entityManager.addEntity(new Rock(handler, 350, 346));
		entityManager.addEntity(new Rock(handler, 594, 697));
		entityManager.addEntity(new Rock(handler, 611, 1100));
		entityManager.addEntity(new Rock(handler, 200, 724));
		entityManager.addEntity(new Rock(handler, 995, 740));
		entityManager.addEntity(new Rock(handler, 787, 278));
		entityManager.addEntity(new Rock(handler, 180, 168));
		entityManager.addEntity(new Goblin(handler, 900, 250));
		entityManager.addEntity(new Goblin(handler, 750, 1315));
		entityManager.addEntity(new Goblin(handler, 497, 1054));
		entityManager.addEntity(new Goblin(handler, 365, 400));
		entityManager.addEntity(new Goblin(handler, 1240, 197));

	}

}