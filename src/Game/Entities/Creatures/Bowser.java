package Game.Entities.Creatures;

import Game.Entities.EntityBase;
import Game.Inventories.Inventory;
import Game.Items.Item;
import Main.Handler;
import Resources.Animation;
import Resources.Images;

import java.awt.*;
import java.util.Random;

/**
 * New enemy found in World 3.
 */
public class Bowser extends CreatureBase {

	private Animation animDown, animUp, animLeft, animRight;

	private Boolean attacking = false;
	boolean rImage = false;
	boolean lImage = false;
	boolean fImage = false;
	boolean bImage = false;

	private int animWalkingSpeed = 150;
	private Inventory bowserInventory;
	private Rectangle bowserCam;

	private int healthcounter = 0;

	private Random randint;
	private int moveCount = 0;
	private int direction;

	public Bowser(Handler handler, float x, float y) {
		super(handler, x, y, CreatureBase.DEFAULT_CREATURE_WIDTH, CreatureBase.DEFAULT_CREATURE_HEIGHT);
		bounds.x = 8 * 2;
		bounds.y = 18 * 2;
		bounds.width = 16 * 2;
		bounds.height = 14 * 2;
		speed = 2.5f;
		health = 8;

		bowserCam = new Rectangle();

		randint = new Random();
		direction = randint.nextInt(4) + 1;

		animDown = new Animation(animWalkingSpeed, Images.bowserFront);
		animLeft = new Animation(animWalkingSpeed, Images.bowserLeft);
		animRight = new Animation(animWalkingSpeed, Images.bowserRight);
		animUp = new Animation(animWalkingSpeed, Images.bowserBack);

		bowserInventory = new Inventory(handler);
	}

	@Override
	public void tick() {
		animDown.tick();
		animUp.tick();
		animRight.tick();
		animLeft.tick();

		moveCount++;
		if (moveCount >= 60) {
			moveCount = 0;
			direction = randint.nextInt(4) + 1;
		}
		checkIfMove();

		move();

		if (isBeinghurt()) {
			healthcounter++;
			if (healthcounter >= 120) {
				setBeinghurt(false);
				System.out.print(isBeinghurt());
			}
		}
		if (healthcounter >= 120 && !isBeinghurt()) {
			healthcounter = 0;
		}

		bowserInventory.tick();

	}

	private void checkIfMove() {
		xMove = 0;
		yMove = 0;

		bowserCam.x = (int) (x - handler.getGameCamera().getxOffset() - (64 * 3));
		bowserCam.y = (int) (y - handler.getGameCamera().getyOffset() - (64 * 3));
		bowserCam.width = 64 * 7;
		bowserCam.height = 64 * 7;

		if (bowserCam.contains(
				handler.getWorld().getEntityManager().getPlayer().getX() - handler.getGameCamera().getxOffset(),
				handler.getWorld().getEntityManager().getPlayer().getY() - handler.getGameCamera().getyOffset())
				|| bowserCam.contains(
						handler.getWorld().getEntityManager().getPlayer().getX() - handler.getGameCamera().getxOffset()
								+ handler.getWorld().getEntityManager().getPlayer().getWidth(),
						handler.getWorld().getEntityManager().getPlayer().getY() - handler.getGameCamera().getyOffset()
								+ handler.getWorld().getEntityManager().getPlayer().getHeight())) {

			Rectangle cb = getCollisionBounds(0, 0);
			Rectangle ar = new Rectangle();
			int arSize = 13;
			ar.width = arSize;
			ar.height = arSize;

			if (lu) {
				ar.x = cb.x + cb.width / 2 - arSize / 2;
				ar.y = cb.y - arSize;
			} else if (ld) {
				ar.x = cb.x + cb.width / 2 - arSize / 2;
				ar.y = cb.y + cb.height;
			} else if (ll) {
				ar.x = cb.x - arSize;
				ar.y = cb.y + cb.height / 2 - arSize / 2;
			} else if (lr) {
				ar.x = cb.x + cb.width;
				ar.y = cb.y + cb.height / 2 - arSize / 2;
			}

			for (EntityBase e : handler.getWorld().getEntityManager().getEntities()) {
				if (e.equals(this))
					continue;
				if (e.getCollisionBounds(0, 0).intersects(ar)
						&& e.equals(handler.getWorld().getEntityManager().getPlayer())) {

					checkAttacks();
					return;
				}
			}

			if (x >= handler.getWorld().getEntityManager().getPlayer().getX() - 8
					&& x <= handler.getWorld().getEntityManager().getPlayer().getX() + 8) {// nada

				xMove = 0;
			} else if (x < handler.getWorld().getEntityManager().getPlayer().getX()) {// move right

				xMove = speed;

			} else if (x > handler.getWorld().getEntityManager().getPlayer().getX()) {// move left

				xMove = -speed;
			}

			if (y >= handler.getWorld().getEntityManager().getPlayer().getY() - 8
					&& y <= handler.getWorld().getEntityManager().getPlayer().getY() + 8) {// nada
				yMove = 0;
			} else if (y < handler.getWorld().getEntityManager().getPlayer().getY()) {// move down
				yMove = speed;

			} else if (y > handler.getWorld().getEntityManager().getPlayer().getY()) {// move up
				yMove = -speed;
			}

		} else {

			switch (direction) {
			case 1:// up
				yMove = -speed;
				break;
			case 2:// down
				yMove = speed;
				break;
			case 3:// left
				xMove = -speed;
				break;
			case 4:// right
				xMove = speed;
				break;

			}
		}
	}

	@Override
	public void render(Graphics g) {
		//Draws Bowser shells in all directions.
		g.drawImage(
				getCurrentAnimationFrame(animDown, animUp, animLeft, animRight, Images.bowserFront, Images.bowserBack,
						Images.bowserLeft, Images.bowserRight),
				(int) (x - handler.getGameCamera().getxOffset()), (int) (y - handler.getGameCamera().getyOffset()),
				width, height, null);
	}

	@Override
	public void die() {
		//Bowser drops a attack boost item.
		handler.getWorld().getItemManager()
				.addItem(Item.boostItem.createNew(((int) x + bounds.x + 10), (int) y + bounds.y, (1)));
	}
}

//if((handler.getWorld().getEntityManager().getPlayer().getX() - this.x > 40 ||
	// this.x - handler.getWorld().getEntityManager().getPlayer().getX() > 40) &&
	// (handler.getWorld().getEntityManager().getPlayer().getY() - this.y >=0 ||
	// this.y - handler.getWorld().getEntityManager().getPlayer().getY() >=0)) {
	// g.drawImage(getCurrentAnimationFrame(animDown,animUp,animLeft,animRight,Images.bowserFront,Images.bowserBack,Images.bowserLeft,Images.bowserRight),
	// (int) (x - handler.getGameCamera().getxOffset()), (int) (y -
	// handler.getGameCamera().getyOffset()), width, height, null);
	// }
	// else if ((handler.getWorld().getEntityManager().getPlayer().getY() - this.y >
	// 50 ||
	// this.y - handler.getWorld().getEntityManager().getPlayer().getY() > 50) &&
	// (handler.getWorld().getEntityManager().getPlayer().getX() - this.x >=0 ||
	// this.x - handler.getWorld().getEntityManager().getPlayer().getX() >=0)) {
	// g.drawImage(getCurrentAnimationFrame(animDown,animUp,animLeft,animRight,Images.bowserFront,Images.bowserBack,Images.bowserLeft,Images.bowserRight),
	// (int) (x - handler.getGameCamera().getxOffset()), (int) (y -
	// handler.getGameCamera().getyOffset()), width, height, null);
	//
	// }
	//
	// ///////////////////////////DRAW BOWSER BACK//////////////////////////////////
	// if(handler.getWorld().getEntityManager().getPlayer().getY() < this.getY() &&
	// (handler.getWorld().getEntityManager().getPlayer().getY() - this.getY() < 10
	// &&
	// this.getY() - handler.getWorld().getEntityManager().getPlayer().getY() < 60)
	//
	// &&
	//
	// (handler.getWorld().getEntityManager().getPlayer().getX() - this.getX() < 40
	// &&
	// handler.getWorld().getEntityManager().getPlayer().getX() - this.getX() >-40
	// ||
	// this.getX() - handler.getWorld().getEntityManager().getPlayer().getX() < 40
	// &&
	// this.getX() - handler.getWorld().getEntityManager().getPlayer().getX() > -40)
	//
	// ) {
	// g.drawImage(Images.bowserBAttack,
	// (int)(x-handler.getGameCamera().getxOffset()+bounds.x -
	// 18),(int)(y-handler.getGameCamera().getyOffset()+(bounds.height/3 - 20)),
	// null);
	//
	//
	//
	// }
	// //////////////////////////////END OF DRAW BOWSER
	// BACK//////////////////////////////
	//
	// //////////////////////////////DRAW BOWSER
	// RIGHT//////////////////////////////////
	// if(handler.getWorld().getEntityManager().getPlayer().getX() > this.getX() &&
	// (handler.getWorld().getEntityManager().getPlayer().getX() - this.getX() < 100
	// &&
	// this.getX() - handler.getWorld().getEntityManager().getPlayer().getX() <10)
	// &&
	// (handler.getWorld().getEntityManager().getPlayer().getY() - this.getY() <20
	// &&
	// handler.getWorld().getEntityManager().getPlayer().getY() - this.getY() >-20
	// ||
	// this.getY() - handler.getWorld().getEntityManager().getPlayer().getY() < 20
	// &&
	// this.getY() - handler.getWorld().getEntityManager().getPlayer().getY() > -20)
	//
	// ) {
	// g.drawImage(Images.bowserRAttack,
	// (int)(x-handler.getGameCamera().getxOffset()+bounds.x -
	// 15),(int)(y-handler.getGameCamera().getyOffset()+(bounds.height/3 - 20)),
	// null);
	//
	//
	// }
	// ///////////////////////// END OF DRAW BOWSER RIGHT///////////////////
	//
	// /////////////////////////DRAW BOWSER LEFT/////////////////////////////
	// if(handler.getWorld().getEntityManager().getPlayer().getX() < this.getX() &&
	// (handler.getWorld().getEntityManager().getPlayer().getX() - this.getX() < 10
	// &&
	// this.getX() - handler.getWorld().getEntityManager().getPlayer().getX() <100 )
	//
	// &&
	//
	// (handler.getWorld().getEntityManager().getPlayer().getY() - this.getY() <15
	// &&
	// handler.getWorld().getEntityManager().getPlayer().getY() - this.getY() >-15
	// ||
	// this.getY() - handler.getWorld().getEntityManager().getPlayer().getY() < 15
	// &&
	// this.getY() - handler.getWorld().getEntityManager().getPlayer().getY() > -15)
	//
	// ) {
	// g.drawImage(Images.bowserLAttack,
	// (int)(x-handler.getGameCamera().getxOffset()+bounds.x -
	// 38),(int)(y-handler.getGameCamera().getyOffset()+(bounds.height/3 - 20)),
	// null);
	//
	// }
	// //////////////////////////END OF DRAW BOWSER
	// LEFT///////////////////////////////
	//
	// //////////////////////////////DRAW BOWSER FRONT//////////////////////////////
	// if(handler.getWorld().getEntityManager().getPlayer().getY() > this.getY() &&
	// (handler.getWorld().getEntityManager().getPlayer().getY() - this.getY() < 60
	// &&
	// this.getY() - handler.getWorld().getEntityManager().getPlayer().getY() < 10)
	//
	// &&
	//
	// (handler.getWorld().getEntityManager().getPlayer().getX() - this.getX() < 40
	// &&
	// handler.getWorld().getEntityManager().getPlayer().getX() - this.getX() >-40
	// ||
	// this.getX() - handler.getWorld().getEntityManager().getPlayer().getX() < 40
	// &&
	// this.getX() - handler.getWorld().getEntityManager().getPlayer().getX() > -40)
	//
	// ) {
	// g.drawImage(Images.bowserFAttack,
	// (int)(x-handler.getGameCamera().getxOffset()+bounds.x -
	// 18),(int)(y-handler.getGameCamera().getyOffset()+(bounds.height/3 - 20)),
	// null);
	//
	// }
	// /////////////////////////END OF DRAW BOWSER FRONT//////////////////////
	//
	// if(isBeinghurt() && healthcounter<=120){
	// g.setColor(Color.white);
	// g.drawString("BowserHealth: " + getHealth(),(int)
	// (x-handler.getGameCamera().getxOffset()),(int)
	// (y-handler.getGameCamera().getyOffset()-20));
	// }
	// }
