package Game.Entities.Statics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import Game.Entities.Creatures.Player;
import Game.Inventories.Inventory;
import Game.Items.Item;
import Main.Handler;
import Resources.Images;

public class Chest extends StaticEntity {
	//Chest object implemented for quest in World 1.
	public Boolean EP = false;
	private Rectangle ir = new Rectangle();
	int amountofSticks = 3; //amount needed to pass quest.
	int amountofCoins = 3; //amount needed to pass quest.
	boolean renderingQuest = false; //Player is standing in front of chest with E pressed.
	boolean questCompleted = false; //Boolean to draw key or not.
	boolean keyInventory = false; //Boolean if key is already in inventory.
	int keysGiven = 0; //Counter for keys given.
	boolean giveKey = true;


	public Chest(Handler handler, float x, float y) {
		super(handler, x, y, 64, 64);
		health=10000000;
		bounds.x=0;
		bounds.y=0;
		bounds.width = 64;
		bounds.height = 64;

		ir.width = bounds.width + 20;
		ir.height = bounds.height ;
		int irx=(int)(bounds.x-handler.getGameCamera().getxOffset()+x );
		int iry= (int)(bounds.y-handler.getGameCamera().getyOffset()+height + 1380);
		ir.y=iry;
		ir.x=irx;

	}

	@Override
	public void tick() {

		if(isBeinghurt()){
			setHealth(10000000);
		}

		if(handler.getKeyManager().attbut){
			EP=true;

		}else if(!handler.getKeyManager().attbut){
			EP=false;
		}

	}

	@Override
	public void render(Graphics g) {
		g.drawImage(Images.chestClose,(int)(x-handler.getGameCamera().getxOffset()), (int) (y-handler.getGameCamera().getyOffset()),width, height,null);

		g.setColor(Color.black);
		checkForPlayer(g, handler.getWorld().getEntityManager().getPlayer());
		for(Item i : handler.getWorld().getEntityManager().getPlayer().getInventory().getInventoryItems()) {
			if(i.getId() == 5 && keysGiven >1) {
				i.setCount(1);
			}
		}

		if(questCompleted && !renderingQuest && giveKey) {
			giveKey = false;
			keysGiven = keysGiven + 1;
			questCompleted = false;
			handler.getWorld().getItemManager().addItem(Item.keyItem.createNew(((int)x + bounds.x + 100),(int)y + bounds.y - 100,(1)));

		}
	}

	private void checkForPlayer(Graphics g, Player p) {
		//Checks if player is in front of Chest and whether or not he is pressing E
		Rectangle pr = p.getCollisionBounds(0,0);
		if(ir.contains(pr) && !EP){
			g.drawImage(Images.E,(int) x+width+5,(int) y -1030 ,32,32,null);
		}else if(ir.contains(pr) && EP){
			renderingQuest = true;
			for(Item i : handler.getWorld().getEntityManager().getPlayer().getInventory().getInventoryItems()) {
				if(i.getId()==3 && amountofSticks>0) {
					amountofSticks = amountofSticks - 1;
					i.setCount(i.getCount()-1);
					continue;
				}
				if(i.getId()==4 && amountofCoins>0) {
					amountofCoins = amountofCoins - 1;
					i.setCount(i.getCount()-1);
					continue;
				}

			}
			if(amountofCoins ==0 && amountofSticks == 0) {
				questCompleted = true;

			}
		}




		if(!ir.contains(pr)) {
			renderingQuest = false;
		}
		if(renderingQuest) { // Draws message showing what must be done to complete quest.
			g.drawImage(Images.chestOpen, (int)(x-handler.getGameCamera().getxOffset()), (int) (y-handler.getGameCamera().getyOffset()),width, height,null);
			g.setColor(Color.BLACK);
			g.fillRect((int)(x-handler.getGameCamera().getxOffset())+150, (int) (y-handler.getGameCamera().getyOffset())-120, 300, 220);
			g.drawImage(Images.stick,(int)(x-handler.getGameCamera().getxOffset())+180, (int) (y-handler.getGameCamera().getyOffset())-70, width-10, height-10, null);
			g.drawImage(Images.coin,(int)(x-handler.getGameCamera().getxOffset())+360, (int) (y-handler.getGameCamera().getyOffset())-70, width-10, height-10, null);
			g.setColor(Color.WHITE);
			g.drawString("You must deliver:", 450, 310);
			g.drawString(Math.max(amountofCoins, 0) + " Coins", (int)(x-handler.getGameCamera().getxOffset())+365, (int) (y-handler.getGameCamera().getyOffset())+10);
			g.drawString(Math.max(amountofSticks, 0) + " Sticks", (int)(x-handler.getGameCamera().getxOffset())+190, (int) (y-handler.getGameCamera().getyOffset())+10);
		}
	}

	@Override
	public void die() {
		// TODO Auto-generated method stub

	}

}
