package Game.Entities.Statics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import Game.Entities.Creatures.Player;
import Main.Handler;
import Resources.Images;
import Worlds.BaseWorld;

public class Sign extends StaticEntity {
	
	//Sign object found in CaveWorld.
	//Specifies quest for said World.
	
	private Rectangle ir = new Rectangle();
	public Boolean EP = false;
	boolean renderingQuest = false;

	public Sign(Handler handler, float x, float y) {
		super(handler, x, y, 64, 56);
		health = 100000000;
		bounds.x = 0;
		bounds.y = 0;
		bounds.width = 64;
		bounds.height = 56;

		ir.width = bounds.width+20;
		ir.height = bounds.height;
		int irx=(int)(bounds.x-handler.getGameCamera().getxOffset()+x);
		int iry= (int)(bounds.y-handler.getGameCamera().getyOffset()+height+130);
		ir.y=iry;
		ir.x=irx;
	}

	@Override
	public void tick() {
		if(isBeinghurt()){
			setHealth(10000000);
		}

		if(handler.getKeyManager().attbut){
			EP=true;

		}else if(!handler.getKeyManager().attbut){
			EP=false;
		}		
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(Images.sign, (int)(x-handler.getGameCamera().getxOffset()), (int) (y-handler.getGameCamera().getyOffset()),width, height,null);
		g.setColor(Color.black);
		checkForPlayer(g, handler.getWorld().getEntityManager().getPlayer());
	}

	private void checkForPlayer(Graphics g, Player p) {
		Rectangle pr = p.getCollisionBounds(0,0);
		if(ir.contains(pr) && !EP){
			g.drawImage(Images.E,(int) x+width+5,(int) y -50 ,32,32,null);
		}else if(ir.contains(pr) && EP) {
			renderingQuest = true;
		}

		if(!ir.contains(pr)) {
			renderingQuest = false;
		}

		if(renderingQuest) {
			g.setColor(Color.BLACK);
			g.fillRect((int)(x-handler.getGameCamera().getxOffset()), (int) (y-handler.getGameCamera().getyOffset())-70, 200, 30);
			g.setColor(Color.WHITE);
			g.drawString("Pick up 5 berries to pass.", 130, 100);
		}
	}


	@Override
	public void die() {
		// TODO Auto-generated method stub

	}

}
