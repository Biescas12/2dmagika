package Game.Tiles;

import java.awt.image.BufferedImage;

public class WaterTile extends Tile { //Water tile found in World 3.

	public WaterTile(BufferedImage texture, int id) {
		super(texture, id);
	}

	@Override
	public boolean isSolid() {
		return true;
	}
}