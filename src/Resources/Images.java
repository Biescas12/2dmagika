package Resources;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by Elemental on 12/19/2016.
 */
public class Images {

	private static final int width = 32, height = 32;
	private static final int Bwidth = 64, Bheight = 64;
	private static final int Rwidth = 56, Rheight = 93;
	private static final int Fwidth = 512, Fheight = 197;
	private static final int Gwidth = 45, Gheight = 65;
	private static final int BGheight = 55;
	private static final int bowserWidth = 71, bowserHeight = 58;
	private static final int bowserStandingW = 84, bowserStandingH = 81;
	private static final int ghostWidth = 32, ghostHeight = 48;

	public static BufferedImage[] blocks;
	public static BufferedImage[] player_right;
	public static BufferedImage[] player_left;
	public static BufferedImage[] player_front;
	public static BufferedImage[] player_back;

	public static BufferedImage[] SkelyEnemy_right;
	public static BufferedImage[] SkelyEnemy_left;
	public static BufferedImage[] SkelyEnemy_front;
	public static BufferedImage[] SkelyEnemy_back;
	public static BufferedImage[] goblinRight;
	public static BufferedImage[] goblinLeft;
	public static BufferedImage[] goblinFront;
	public static BufferedImage[] goblinBack;
	public static BufferedImage[] bowserFront;
	public static BufferedImage[] bowserRight;
	public static BufferedImage[] bowserLeft;
	public static BufferedImage[] bowserBack;
	public static BufferedImage bowserRAttack;
	public static BufferedImage bowserLAttack;
	public static BufferedImage bowserFAttack;
	public static BufferedImage bowserBAttack;
	public static BufferedImage[] ghostFront;
	public static BufferedImage[] ghostBack;
	public static BufferedImage[] ghostRight;
	public static BufferedImage[] ghostLeft;

	public static BufferedImage[] butstart;
	public static BufferedImage[] particleSmoke;
	public static BufferedImage[] items;
	public static BufferedImage[] numbers;
	public static BufferedImage inventory;
	public static BufferedImage title;
	public static BufferedImage door;
	public static BufferedImage E;
	public static BufferedImage EP;
	public static BufferedImage Pause;
	public static BufferedImage gameOver;
	public static BufferedImage gameWin;
	public static BufferedImage[] Resume;
	public static BufferedImage[] BTitle;
	public static BufferedImage[] Options;
	public static BufferedImage[] Runes;
	public static ImageIcon icon;
	public static BufferedImage[] FireBallLeft;
	public static BufferedImage[] FireBallRight;
	public static BufferedImage[] FireBallUp;
	public static BufferedImage[] FireBallDown;
	public static BufferedImage loading;
	public static BufferedImage spellGUI;
	public static BufferedImage bush;
	public static BufferedImage stick;
	public static BufferedImage coin;
	public static BufferedImage chestOpen;
	public static BufferedImage chestClose;
	public static BufferedImage key;
	public static BufferedImage berry;
	public static BufferedImage sign;
	public static BufferedImage potion;
	public static BufferedImage boost;

	public Images() {

		SpriteSheet newsheet = new SpriteSheet(Images.loadImage("/Sheets/SpriteSheet.png"));
		SpriteSheet numsheet = new SpriteSheet(Images.loadImage("/Sheets/numsheet.png"));
		SpriteSheet runesheet = new SpriteSheet(Images.loadImage("/Sheets/runes.png"));
		SpriteSheet FireBallsheet = new SpriteSheet(Images.loadImage("/Sheets/FireBall.png"));
		SpriteSheet FireBallRightsheet = new SpriteSheet(Images.loadImage("/Sheets/FireBallRight.png"));
		SpriteSheet FireBallUpsheet = new SpriteSheet(Images.loadImage("/Sheets/FireBallUp.png"));
		SpriteSheet FireBallDownsheet = new SpriteSheet(Images.loadImage("/Sheets/FireBallDown.png"));
		SpriteSheet goblinSheet = new SpriteSheet(Images.loadImage("/Sheets/goblinSpriteImage.png"));
		SpriteSheet bowserSheet = new SpriteSheet(Images.loadImage("/Sheets/bowserSprite.png"));
		SpriteSheet ghostSheet = new SpriteSheet(Images.loadImage("/Sheets/ghostSprite.png"));
		SpriteSheet ghostSheet2 = new SpriteSheet(Images.loadImage("/Sheets/ghostSprite2.png"));

		blocks = new BufferedImage[16];

		player_left = new BufferedImage[4];
		player_right = new BufferedImage[4];
		player_front = new BufferedImage[4];
		player_back = new BufferedImage[4];

		SkelyEnemy_left = new BufferedImage[4];
		SkelyEnemy_right = new BufferedImage[4];
		SkelyEnemy_front = new BufferedImage[4];
		SkelyEnemy_back = new BufferedImage[4];

		goblinRight = new BufferedImage[3];
		goblinLeft = new BufferedImage[3];
		goblinFront = new BufferedImage[3];
		goblinBack = new BufferedImage[3];

		bowserRight = new BufferedImage[3];
		bowserLeft = new BufferedImage[3];
		bowserFront = new BufferedImage[3];
		bowserBack = new BufferedImage[3];

		ghostRight = new BufferedImage[2];
		ghostLeft = new BufferedImage[2];
		ghostFront = new BufferedImage[3];
		ghostBack = new BufferedImage[3];

		butstart = new BufferedImage[3];
		particleSmoke = new BufferedImage[3];
		items = new BufferedImage[3];
		numbers = new BufferedImage[21];
		Resume = new BufferedImage[2];
		BTitle = new BufferedImage[2];
		Options = new BufferedImage[2];
		Runes = new BufferedImage[36];

		FireBallLeft = new BufferedImage[6];
		FireBallRight = new BufferedImage[6];
		FireBallUp = new BufferedImage[6];
		FireBallDown = new BufferedImage[6];

		try {

			loading = ImageIO.read(getClass().getResourceAsStream("/Sheets/loading.png"));
			spellGUI = ImageIO.read(getClass().getResourceAsStream("/Sheets/SpellGUI.png"));

			inventory = ImageIO.read(getClass().getResourceAsStream("/Sheets/guit.png"));
			title = ImageIO.read(getClass().getResourceAsStream("/Sheets/Title.png"));
			door = ImageIO.read(getClass().getResourceAsStream("/Sheets/Door.png"));
			E = ImageIO.read(getClass().getResourceAsStream("/Buttons/E.png"));
			EP = ImageIO.read(getClass().getResourceAsStream("/Buttons/EP.png"));
			Pause = ImageIO.read(getClass().getResourceAsStream("/Buttons/Pause.png"));
			Resume[0] = ImageIO.read(getClass().getResourceAsStream("/Buttons/Resume.png"));
			Resume[1] = ImageIO.read(getClass().getResourceAsStream("/Buttons/ResumeP.png"));
			BTitle[0] = ImageIO.read(getClass().getResourceAsStream("/Buttons/BTitle.png"));
			BTitle[1] = ImageIO.read(getClass().getResourceAsStream("/Buttons/BTitleP.png"));
			Options[0] = ImageIO.read(getClass().getResourceAsStream("/Buttons/Options.png"));
			Options[1] = ImageIO.read(getClass().getResourceAsStream("/Buttons/OptionsP.png"));
			bush = ImageIO.read(getClass().getResourceAsStream("/Sheets/bush.png"));
			stick = ImageIO.read(getClass().getResourceAsStream("/Sheets/stick.png"));
			coin = ImageIO.read(getClass().getResourceAsStream("/Sheets/coin.png"));
			chestOpen = ImageIO.read(getClass().getResourceAsStream("/Sheets/chestOpen.png"));
			chestClose = ImageIO.read(getClass().getResourceAsStream("/Sheets/chestClose.png"));
			key = ImageIO.read(getClass().getResourceAsStream("/Sheets/key.png"));
			berry = ImageIO.read(getClass().getResourceAsStream("/Sheets/Berry.png"));
			sign = ImageIO.read(getClass().getResourceAsStream("/Sheets/Sign.png"));
			potion = ImageIO.read(getClass().getResourceAsStream("/Sheets/potion.png"));
			gameOver = ImageIO.read(getClass().getResourceAsStream("/Sheets/gameOver.png"));
			gameWin = ImageIO.read(getClass().getResourceAsStream("/Sheets/win.png"));
			boost = ImageIO.read(getClass().getResourceAsStream("/Sheets/star.jpg"));
			// icon
			icon = new ImageIcon(runesheet.crop(Rwidth * 1, Rheight * 0, Rwidth, Rheight));

			// fireball left
			FireBallLeft[0] = FireBallsheet.crop(Fwidth * 0, Fheight * 0, Fwidth, Fheight);
			FireBallLeft[1] = FireBallsheet.crop(Fwidth * 1, Fheight * 0, Fwidth, Fheight);
			FireBallLeft[2] = FireBallsheet.crop(Fwidth * 2, Fheight * 0, Fwidth, Fheight);
			FireBallLeft[3] = FireBallsheet.crop(Fwidth * 0, Fheight * 1, Fwidth, Fheight);
			FireBallLeft[4] = FireBallsheet.crop(Fwidth * 1, Fheight * 1, Fwidth, Fheight);
			FireBallLeft[5] = FireBallsheet.crop(Fwidth * 2, Fheight * 1, Fwidth, Fheight);

			// fireball right
			FireBallRight[0] = FireBallRightsheet.crop(Fwidth * 0, Fheight * 0, Fwidth, Fheight);
			FireBallRight[1] = FireBallRightsheet.crop(Fwidth * 1, Fheight * 0, Fwidth, Fheight);
			FireBallRight[2] = FireBallRightsheet.crop(Fwidth * 2, Fheight * 0, Fwidth, Fheight);
			FireBallRight[3] = FireBallRightsheet.crop(Fwidth * 0, Fheight * 1, Fwidth, Fheight);
			FireBallRight[4] = FireBallRightsheet.crop(Fwidth * 1, Fheight * 1, Fwidth, Fheight);
			FireBallRight[5] = FireBallRightsheet.crop(Fwidth * 2, Fheight * 1, Fwidth, Fheight);

			// fireball up
			FireBallUp[0] = FireBallUpsheet.crop(Fheight * 1, Fwidth * 0, Fheight, Fwidth);
			FireBallUp[1] = FireBallUpsheet.crop(Fheight * 1, Fwidth * 1, Fheight, Fwidth);
			FireBallUp[2] = FireBallUpsheet.crop(Fheight * 1, Fwidth * 2, Fheight, Fwidth);
			FireBallUp[3] = FireBallUpsheet.crop(Fheight * 0, Fwidth * 0, Fheight, Fwidth);
			FireBallUp[4] = FireBallUpsheet.crop(Fheight * 0, Fwidth * 1, Fheight, Fwidth);
			FireBallUp[5] = FireBallUpsheet.crop(Fheight * 0, Fwidth * 2, Fheight, Fwidth);

			// fireball down
			FireBallDown[0] = FireBallDownsheet.crop(Fheight * 1, Fwidth * 0, Fheight, Fwidth);
			FireBallDown[1] = FireBallDownsheet.crop(Fheight * 1, Fwidth * 1, Fheight, Fwidth);
			FireBallDown[2] = FireBallDownsheet.crop(Fheight * 1, Fwidth * 2, Fheight, Fwidth);
			FireBallDown[3] = FireBallDownsheet.crop(Fheight * 0, Fwidth * 0, Fheight, Fwidth);
			FireBallDown[4] = FireBallDownsheet.crop(Fheight * 0, Fwidth * 1, Fheight, Fwidth);
			FireBallDown[5] = FireBallDownsheet.crop(Fheight * 0, Fwidth * 2, Fheight, Fwidth);

			Runes[0] = runesheet.crop(Rwidth * 0, Rheight * 0, Rwidth, Rheight);// Runes
			Runes[1] = runesheet.crop(Rwidth * 1, Rheight * 0, Rwidth, Rheight);
			Runes[2] = runesheet.crop(Rwidth * 2, Rheight * 0, Rwidth, Rheight);
			Runes[3] = runesheet.crop(Rwidth * 3, Rheight * 0, Rwidth, Rheight);
			Runes[4] = runesheet.crop(Rwidth * 4, Rheight * 0, Rwidth, Rheight);
			Runes[5] = runesheet.crop(Rwidth * 5, Rheight * 0, Rwidth, Rheight);
			Runes[6] = runesheet.crop(Rwidth * 6, Rheight * 0, Rwidth, Rheight);
			Runes[7] = runesheet.crop(Rwidth * 7, Rheight * 0, Rwidth, Rheight);

			blocks[0] = ImageIO.read(getClass().getResourceAsStream("/Blocks/Slime.png"));

			butstart[0] = newsheet.crop(11, 422, 93, 34);// normbut
			butstart[1] = newsheet.crop(11, 456, 93, 33);// hoverbut
			butstart[2] = newsheet.crop(11, 489, 93, 32);// clickbut

			particleSmoke[0] = newsheet.crop(111, 397, 18, 38);
			particleSmoke[1] = newsheet.crop(129, 399, 20, 35);
			particleSmoke[2] = newsheet.crop(154, 400, 20, 35);

			items[0] = newsheet.crop(114, 448, 30, 24);// log

			numbers[1] = numsheet.crop(17, 15, 17, 22);
			numbers[2] = numsheet.crop(64, 16, 14, 19);
			numbers[3] = numsheet.crop(110, 16, 14, 19);
			numbers[4] = numsheet.crop(154, 17, 15, 19);
			numbers[5] = numsheet.crop(19, 61, 13, 20);
			numbers[6] = numsheet.crop(64, 61, 14, 20);
			numbers[7] = numsheet.crop(110, 62, 14, 19);
			numbers[8] = numsheet.crop(156, 61, 13, 20);
			numbers[9] = numsheet.crop(19, 107, 13, 20);
			numbers[10] = numsheet.crop(60, 107, 24, 20);
			numbers[11] = numsheet.crop(107, 107, 20, 19);
			numbers[12] = numsheet.crop(151, 107, 23, 19);
			numbers[13] = numsheet.crop(14, 152, 23, 20);
			numbers[14] = numsheet.crop(60, 152, 23, 20);
			numbers[15] = numsheet.crop(105, 153, 24, 20);
			numbers[16] = numsheet.crop(151, 153, 23, 20);
			numbers[17] = numsheet.crop(14, 198, 24, 20);
			numbers[18] = numsheet.crop(60, 198, 23, 20);
			numbers[19] = numsheet.crop(106, 198, 23, 21);
			numbers[20] = numsheet.crop(149, 198, 28, 20);

			// block images,array index is equal to block id
			blocks[1] = newsheet.crop(0, 324, Bwidth, Bheight);// grass
			blocks[2] = newsheet.crop(67, 260, Bwidth, Bheight);// dirt
			blocks[3] = newsheet.crop(67, 324, Bwidth, Bheight);// dirtrock
			blocks[4] = newsheet.crop(0, 0, Bwidth, Bheight);// uppperleft
			blocks[5] = newsheet.crop(67, 0, Bwidth, Bheight);// upperright
			blocks[6] = newsheet.crop(67, 65, Bwidth, Bheight);// lowerleft
			blocks[7] = newsheet.crop(0, 65, Bwidth, Bheight);// lowerright
			blocks[8] = newsheet.crop(0, 195, Bwidth, Bheight);// leftwall
			blocks[9] = newsheet.crop(67, 195, Bwidth, Bheight);// rightwall
			blocks[10] = newsheet.crop(0, 130, Bwidth, Bheight);// topwall
			blocks[11] = newsheet.crop(67, 130, Bwidth, Bheight);// lowerwall
			blocks[12] = newsheet.crop(0, 260, Bwidth, Bheight);// mossyrock
			blocks[13] = newsheet.crop(176, 0, Bwidth, Bheight * 2);// tree
			blocks[14] = newsheet.crop(174, 410, 78, 74);// rock
			blocks[15] = ImageIO.read(getClass().getResourceAsStream("/Blocks/waterTile.png"));

			// player anim
			player_front[0] = newsheet.crop(132, 131, width, height);
			player_front[1] = newsheet.crop(164, 131, width, height);
			player_front[2] = newsheet.crop(196, 131, width, height);
			player_front[3] = newsheet.crop(228, 131, 28, height);

			player_left[0] = newsheet.crop(132, 163, width, height);
			player_left[1] = newsheet.crop(164, 163, width, height);
			player_left[2] = newsheet.crop(196, 163, width, height);
			player_left[3] = newsheet.crop(228, 163, 28, height);

			player_right[0] = newsheet.crop(132, 195, width, height);
			player_right[1] = newsheet.crop(164, 195, width, height);
			player_right[2] = newsheet.crop(196, 195, width, height);
			player_right[3] = newsheet.crop(228, 195, 28, height);

			player_back[0] = newsheet.crop(132, 227, width, height);
			player_back[1] = newsheet.crop(164, 227, width, height);
			player_back[2] = newsheet.crop(196, 227, width, height);
			player_back[3] = newsheet.crop(228, 227, 28, height);

			// Skely enemy anim
			SkelyEnemy_front[0] = newsheet.crop(132, 131 + 130, width, height);
			SkelyEnemy_front[1] = newsheet.crop(164, 131 + 130, width, height);
			SkelyEnemy_front[2] = newsheet.crop(196, 131 + 130, width, height);
			SkelyEnemy_front[3] = newsheet.crop(228, 131 + 130, 28, height);

			SkelyEnemy_left[0] = newsheet.crop(132, 163 + 130, width, height);
			SkelyEnemy_left[1] = newsheet.crop(164, 163 + 130, width, height);
			SkelyEnemy_left[2] = newsheet.crop(196, 163 + 130, width, height);
			SkelyEnemy_left[3] = newsheet.crop(228, 163 + 130, 28, height);

			SkelyEnemy_right[0] = newsheet.crop(132, 195 + 130, width, height);
			SkelyEnemy_right[1] = newsheet.crop(164, 195 + 130, width, height);
			SkelyEnemy_right[2] = newsheet.crop(196, 195 + 130, width, height);
			SkelyEnemy_right[3] = newsheet.crop(228, 195 + 130, 28, height);

			SkelyEnemy_back[0] = newsheet.crop(132, 227 + 130, width, height);
			SkelyEnemy_back[1] = newsheet.crop(164, 227 + 130, width, height);
			SkelyEnemy_back[2] = newsheet.crop(196, 227 + 130, width, height);
			SkelyEnemy_back[3] = newsheet.crop(228, 227 + 130, 28, height);

			// goblin enemy
			goblinRight[0] = goblinSheet.crop(400, 68, Gwidth, Gheight);
			goblinRight[1] = goblinSheet.crop(70, 68, Gwidth, Gheight);
			goblinRight[2] = goblinSheet.crop(202, 68, Gwidth, Gheight);

			goblinLeft[0] = goblinSheet.crop(415, 196, Gwidth, Gheight);
			goblinLeft[1] = goblinSheet.crop(80, 196, Gwidth, Gheight);
			goblinLeft[2] = goblinSheet.crop(277, 196, Gwidth, Gheight);

			goblinBack[0] = goblinSheet.crop(402, 140, Gwidth, BGheight);
			goblinBack[1] = goblinSheet.crop(78, 140, Gwidth, BGheight);
			goblinBack[2] = goblinSheet.crop(272, 140, Gwidth, BGheight);

			goblinFront[0] = goblinSheet.crop(402, 0, Gwidth, Gheight);
			goblinFront[1] = goblinSheet.crop(80, 0, Gwidth, Gheight);
			goblinFront[2] = goblinSheet.crop(337, 0, Gwidth, Gheight);

			bowserRight[0] = bowserSheet.crop(10, 374, bowserWidth, bowserHeight);
			bowserRight[1] = bowserSheet.crop(90, 374, bowserWidth, bowserHeight);
			bowserRight[2] = bowserSheet.crop(180, 374, bowserWidth, bowserHeight);
			bowserRAttack = bowserSheet.crop(682, 522, bowserStandingW, bowserStandingH);

			bowserLeft[0] = bowserSheet.crop(10, 374, bowserWidth, bowserHeight);
			bowserLeft[1] = bowserSheet.crop(90, 374, bowserWidth, bowserHeight);
			bowserLeft[2] = bowserSheet.crop(180, 374, bowserWidth, bowserHeight);
			bowserLAttack = bowserSheet.crop(680, 430, bowserStandingW, bowserStandingH);

			bowserFront[0] = bowserSheet.crop(10, 374, bowserWidth, bowserHeight);
			bowserFront[1] = bowserSheet.crop(90, 374, bowserWidth, bowserHeight);
			bowserFront[2] = bowserSheet.crop(180, 374, bowserWidth, bowserHeight);
			bowserFAttack = bowserSheet.crop(430, 13, bowserStandingW, bowserStandingH);

			bowserBack[0] = bowserSheet.crop(10, 374, bowserWidth, bowserHeight);
			bowserBack[1] = bowserSheet.crop(90, 374, bowserWidth, bowserHeight);
			bowserBack[2] = bowserSheet.crop(180, 374, bowserWidth, bowserHeight);
			bowserBAttack = bowserSheet.crop(88, 265, bowserStandingW, bowserStandingH);

			ghostFront[0] = ghostSheet.crop(0, 0, ghostWidth, ghostHeight);
			ghostFront[1] = ghostSheet.crop(50, 0, ghostWidth, ghostHeight);
			ghostFront[2] = ghostSheet.crop(95, 0, ghostWidth, ghostHeight);

			ghostBack[0] = ghostSheet.crop(143, 0, ghostWidth, ghostHeight);
			ghostBack[1] = ghostSheet.crop(143, 0, ghostWidth, ghostHeight);
			ghostBack[2] = ghostSheet.crop(240, 0, ghostWidth, ghostHeight);

			ghostLeft[0] = ghostSheet.crop(290, 0, ghostWidth, ghostHeight);
			ghostLeft[1] = ghostSheet.crop(336, 0, ghostWidth, ghostHeight);

			ghostRight[0] = ghostSheet2.crop(50, 0, ghostWidth, ghostHeight);
			ghostRight[1] = ghostSheet2.crop(100, 0, ghostWidth, ghostHeight);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static BufferedImage loadImage(String path) {
		try {
			return ImageIO.read(Images.class.getResourceAsStream(path));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

}